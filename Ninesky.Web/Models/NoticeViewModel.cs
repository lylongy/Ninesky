﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ninesky.Web.Models
{
    /// <summary>
    /// 消息提示
    /// </summary>
    public class NoticeViewModel
    {

        public NoticeType Type { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public Dictionary<string, string> Links { get; set; }
    }

    /// <summary>
    /// 提示类型
    /// </summary>
    public enum NoticeType
    {
        [Display(Name = "成功")]
        succee,
        [Display(Name = "失败")]
        faile,
        [Display(Name = "消息")]
        info,
        [Display(Name = "警告")]
        warning,
    }
}
