﻿using Ninesky.Entities;
using Ninesky.InterfaceService;

namespace Ninesky.Service
{
    public class ArticleService:BaseService<Article>,InterfaceArticleService
    {
        public ArticleService(NineskyDbContext nineskyDbContext) : base(nineskyDbContext) { }
    }
}
