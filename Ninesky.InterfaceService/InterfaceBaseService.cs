﻿using System;
using System.Threading.Tasks;
using Ninesky.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace Ninesky.InterfaceService
{
    public interface InterfaceBaseService<T> where T :class
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        Task<OperationResult> AddAsync(T entity);

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns></returns>
        Task<bool> AnyAsync(Expression<Func<T, bool>> expression);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        Task<OperationResult> UpdateAsync(T entity);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        Task<OperationResult> DeleteAsync(T entity);

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        Task<T> FindAsync(params object[] keyValues);

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns></returns>
        IQueryable<T> FindList(Expression<Func<T, bool>> expression);

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <param name="orderByString">排序。支持多条件（"属性名,属性名 desc"）</param>
        /// <returns></returns>
        IQueryable<T> FindList(Expression<Func<T, bool>> expression, string orderByString);
    }
}
