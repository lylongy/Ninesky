﻿using System.ComponentModel.DataAnnotations;

namespace Ninesky.Entities
{
    /// <summary>
    /// 栏目模型
    /// </summary>
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        /// <summary>
        /// 栏目类型
        /// </summary>
        public CategoryType Type { get; set; }

        /// <summary>
        /// 父栏目Id
        /// <para>根节点的值为0</para>
        /// </summary>
        [Required]
        public int ParentId { get; set; }

        /// <summary>
        /// 父栏目路径
        /// <para>根节点的值为0，子节点的值为：0,1,6,76</para>
        /// </summary>
        [Required]
        [StringLength(255)]
        public string ParentPath { get; set; }

        /// <summary>
        /// 栏目深度
        /// <para>根节点的值为0，子节点的值为该节点所在的层数</para>
        /// </summary>
        [Required]
        public int Depth { get; set; }

        /// <summary>
        /// 排序
        /// <para>数字越小排序越靠前</para>
        /// </summary>
        [Required]
        public int Order { get; set; }
        /// <summary>
        /// 栏目名称
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// 搜索引擎关键字
        /// </summary>
        [StringLength(255)]
        public string Meta_Keywords { get; set; }

        /// <summary>
        /// 搜索引擎的描述
        /// </summary>
        [StringLength(255)]
        public string Meta_Description { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        [StringLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// 内容类型
        /// </summary>
        public ContentType ContentType { get; set; }
        /// <summary>
        /// 栏目首页模板
        /// </summary>
        [StringLength(255)]
        public string HomeTemplate { get; set; }

        /// <summary>
        /// 列表页模板
        /// </summary>
        [StringLength(255)]
        public string ListTemplate { get; set; }

        /// <summary>
        /// 内容页模板
        /// </summary>
        [StringLength(255)]
        public string ContentTemplate { get; set; }

        /// <summary>
        /// 链接地址
        /// </summary>
        [StringLength(255)]
        public string LinkUrl { get; set; }
    }
}
