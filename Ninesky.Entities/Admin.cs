﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ninesky.Entities
{
    public class Admin
    {
        [Key]
        public int AdminId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [StringLength(255)]
        public string Password { get; set; }

        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime LastLoginTime { get; set; }

        /// <summary>
        /// 最后登录IP
        /// </summary>
        [StringLength(255)]
        public string LastLoginIP { get; set; }

        /// <summary>
        /// 是否锁定
        /// </summary>
        public bool IsLock { get; set; }

        /// <summary>
        /// 登录错误次数
        /// </summary>
        public int LoginErrorTimes { get; set; }
    }
}
