﻿using System.Collections.Generic;

namespace Ninesky.Models
{
    /// <summary>
    /// 栏目树形节点
    /// </summary>
    public class CategoryTreeNode
    {
        public string Title { get; set; }

        /// <summary>
        /// 值【id】
        /// </summary>
        public int Value { get; set; }
        /// <summary>
        /// 栏目类型
        /// </summary>
        public string Type { get; set; }

        public bool Expand { get; set; }
        public List<CategoryTreeNode> Children { get; set; }

        public CategoryTreeNode()
        {
            Children = new List<CategoryTreeNode>();
        }
    }
}
