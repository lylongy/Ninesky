﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Ninesky.Entities;
using System.Collections.Generic;

namespace Ninesky.Models
{
    public class CategoryModelView:Category
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public new  CategoryType Type { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public new ContentType ContentType { get; set; }

        public List<string> ParentIds { get {
                List<string> idList= new List<string>();
                if (!string.IsNullOrEmpty(ParentPath))
                {
                    var idStrArry = ParentPath.Split(',');
                    foreach(var idStr in idStrArry)
                    {
                        idList.Add(idStr);
                    }
                    
                }
                else idList.Add("0");
                return idList;
            } }

        public CategoryModelView(Category category)
        {
            CategoryId = category.CategoryId;
            ContentTemplate = category.ContentTemplate;
            ContentType = category.ContentType;
            Depth = category.Depth;
            Description = category.Description;
            HomeTemplate = category.HomeTemplate;
            LinkUrl = category.LinkUrl;
            ListTemplate = category.ListTemplate;
            Meta_Description = category.Meta_Description;
            Meta_Keywords = category.Meta_Keywords;
            Name = category.Name;
            Order = category.Order;
            ParentId = category.ParentId;
            ParentPath = category.ParentPath;
            Type = category.Type;
        }
    }
}
